from scipy import signal
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Sampling rate 1000 hz / second
t = np.linspace(0, 1, 1000, endpoint=True)

#Signal1
df1 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv", index_col=['Signal_1'], parse_dates=['Signal_1'])
pwm1 = signal.square(df1, duty=0.5)

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.subplot(2, 1, 1)
plt.plot(t, df1)
plt.subplot(2, 1, 2)
plt.plot(t, pwm1)
plt.ylim(-2, 2)

plt.savefig('Sig1_squarewave.jpg')
plt.show()

#Signal2
df2 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv", index_col=['Signal_2'], parse_dates=['Signal_2'])
pwm2 = signal.square(df2, duty=0.5)

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.subplot(2, 1, 1)
plt.plot(t, df2)
plt.subplot(2, 1, 2)
plt.plot(t, pwm2)
plt.ylim(-2, 2)

plt.savefig('Sig2_squarewave.jpg')
plt.show()