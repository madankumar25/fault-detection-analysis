from scipy import signal
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

dataframe = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv")

from sklearn.preprocessing import normalize
data_scaled = normalize(dataframe)
data_scaled = pd.DataFrame(data_scaled, columns=dataframe.columns)
print(data_scaled.head())

data_scale = dataframe.drop(columns=['no', 'Signal_1'])

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

x = dataframe.no
y = dataframe.Signal_1
plt.scatter(x,y)

plt.title('Detect Oscillations')
plt.ylabel('Signal 1')

plt.savefig('Out1.jpg')
plt.show()

data_scale = dataframe.drop(columns=['no', 'Signal_2'])

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

x = dataframe.no
y = dataframe.Signal_2
plt.scatter(x,y)

plt.title('Detect Oscillations')
plt.ylabel('Signal 2')

plt.savefig('Out2.jpg')
plt.show()

from scipy.fftpack import fft, ifft

ffill_data = dataframe.fillna(method='ffill')
print(ffill_data.head())

df1 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv", index_col=['Signal_1'], parse_dates=['Signal_1'])

#First Method
N1 = 999
T1 = 1
x1 = np.linspace(0, N1*T1, N1)
y1 = df1

yf1 = fft(y1)
print(yf1)

xf1 = np.linspace(0.0, 1.0/(2.0*T1), N1//2)
print(xf1)

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(xf1, yf1[0:N1//2], label = 'Signal_1')
plt.grid()

plt.savefig('FFT_Sig1.jpg')
plt.show()

#Second Method
L = np.array(df1)
L = np.round(L, 1)
# Remove DC component
L -= np.mean(L)
# Window signal
#L *= scipy.signal.windows.hann(len(L))

fft = np.fft.rfft(L, norm="ortho")

def abs2(x):
    return x.real**2 + x.imag**2

selfconvol=np.fft.irfft(abs2(fft), norm="ortho")
selfconvol=selfconvol/selfconvol[0]

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(selfconvol)

plt.savefig('SelfConvol.jpg')
plt.show()

#Third Method
df1n = df1 - np.mean(df1);

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(df1n)

plt.savefig('Signal_1.jpg')
plt.show()

df2 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv", index_col=['Signal_2'], parse_dates=['Signal_2'])

df2n = df2 - np.mean(df2);

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(df2n)
plt.savefig('Signal_2.jpg')
plt.show()

#Fourth Method
#sig1 = np.fft.fftfreq(df1)
#print(sig1)
sig1n = np.fft.fftshift(df1)

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(sig1n)

plt.savefig('Sig1_fftshift.jpg')
plt.show()


#Plot Signal1
plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(abs(df1))

plt.savefig('Sig1_display.jpg')
plt.show()

'''
plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(sig1)

plt.savefig('Sig1_fftshift.jpg')
plt.show()
'''

'''
Square Wave Plotter
'''

# Sampling rate 1000 hz / second
t = np.linspace(0, 1, 1000, endpoint=True)

#Signal1
df1 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv", index_col=['Signal_1'], parse_dates=['Signal_1'])
pwm1 = signal.square(df1, duty=0.5)

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.subplot(2, 1, 1)
plt.plot(t, df1)
plt.subplot(2, 1, 2)
plt.plot(t, pwm1)
plt.ylim(-2, 2)

plt.savefig('Sig1_squarewave.jpg')
plt.show()

#Signal2
df2 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data.csv", index_col=['Signal_2'], parse_dates=['Signal_2'])
pwm2 = signal.square(df2, duty=0.5)

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.subplot(2, 1, 1)
plt.plot(t, df2)
plt.subplot(2, 1, 2)
plt.plot(t, pwm2)
plt.ylim(-2, 2)

plt.savefig('Sig2_squarewave.jpg')
plt.show()