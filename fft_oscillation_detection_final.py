from scipy.fftpack import fft, ifft
import scipy
from scipy.signal import argrelextrema
from scipy import signal
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Sampling rate 1000 hz / second
t = np.linspace(0, 1, 1000, endpoint=True)

#Signal1_Data
df1 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data1.csv", index_col=['Signal_1'], parse_dates=['Signal_1'])

#Signal2_Data
df2 = pd.read_csv("//Users//madankumar//Desktop//Process_Temperature_Readings//Data1.csv", index_col=['Signal_2'], parse_dates=['Signal_2'])
############################################################################

'''
Signal1 Analysis
'''

#Signal1_FFT
N1 = df1.size
T1 = 1.0 / 25000.0
x1 = np.linspace(0, N1*T1, N1)
y1 = df1 

yf1 = fft(y1)
print(yf1)

xf1 = np.linspace(0.0, 1.0/(2.0*T1), N1//2)
print(xf1)

#Plot Signal1_FFT
plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(yf1, label = 'Signal_1')
plt.grid()

plt.savefig('Signal1_FFT.jpg')
plt.show()

#Plot Signal1_FFT_OP
plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(xf1, yf1[0:N1//2], label = 'Signal_1')
plt.grid()

plt.savefig('Signal1_OP.jpg')
plt.show()

#Signal1_Peak
m = argrelextrema(yf1, np.greater)
sig1_g = [yf1[m] for i in m]

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(yf1)
plt.grid()

plt.savefig('Signal1_Peak.jpg')
plt.show()

#Signal1_Square_Wave
pwm1 = signal.square(df1, duty=0.5)

y1 = df1
yf1 = fft(y1)

plt.figure(figsize=(12, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.subplot(3, 1, 1)
plt.plot(t, df1)
plt.subplot(3, 1, 2)
plt.plot(t, yf1)
plt.subplot(3, 1, 3)
plt.plot(t, pwm1)
plt.ylim(-2, 2)

plt.savefig('Signal1_Squarewave.jpg')
plt.show()
############################################################################

'''
Signal2 Analysis
'''

#Signal2_FFT
N2 = df2.size
T2 = 1.0 / 25000.0
x2 = np.linspace(0, N2*T2, N2)
y2 = df2

yf2 = fft(y2)
print(yf2)

xf2 = np.linspace(0.0, 1.0/(2.0*T2), N2//2)
print(xf2)

#Plot Signal2_FFT
plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(yf2, label = 'Signal_2')
plt.grid()

plt.savefig('Signal2_FFT.jpg')
plt.show()

#Plot Signal2_FFT_OP
plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(xf2, yf2[0:N2//2], label = 'Signal_2')
plt.grid()

plt.savefig('Signal2_OP.jpg')
plt.show()

#Signal2_Peak
m = argrelextrema(yf2, np.greater)
sig2_g = [yf1[m] for i in m]

plt.figure(figsize=(10, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.plot(yf2)
plt.grid()

plt.savefig('Signal2_Peak.jpg')
plt.show()

#Signal2_Square_Wave
pwm2 = signal.square(df2, duty=0.5)

y2 = df2
yf2 = fft(y2)

plt.figure(figsize=(12, 7))
plt.xticks(rotation=45, ha='right')
plt.subplots_adjust(bottom=0.30)

plt.subplot(3, 1, 1)
plt.plot(t, df2)
plt.subplot(3, 1, 2)
plt.plot(t, yf2)
plt.subplot(3, 1, 3)
plt.plot(t, pwm2)
plt.ylim(-2, 2)

plt.savefig('Signal2_Squarewave.jpg')
plt.show()
############################################################################